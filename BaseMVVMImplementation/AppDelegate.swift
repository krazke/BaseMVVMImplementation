//
//  AppDelegate.swift
//  BaseMVVMImplementation
//
//  Created by krazke on 2/7/19.
//  Copyright © 2019 krazke.corp. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var services: BaseServicesConfiguration?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        services = BaseServicesConfiguration()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .white
        window?.rootViewController = UIViewController()
        
        setupInitialController()
        
        window?.makeKeyAndVisible()
        
        return true
    }
    
    func setupInitialController() {
        let sceneCoordinator = SceneCoordinator(window: window!)
        
        let firstViewModel = FirstViewModel(services: services!, sceneCoordinator: sceneCoordinator)
        let firstScene = Scene.first(firstViewModel)
        let navigationViewModel = BaseNavigationControllerModel(services: services!, sceneCoordinator: sceneCoordinator, root: firstScene, config: .regular)
        let navigationScene = Scene.navigation(navigationViewModel)
        
        sceneCoordinator.transition(to: navigationScene, type: .root, animated: false)
    }
}

