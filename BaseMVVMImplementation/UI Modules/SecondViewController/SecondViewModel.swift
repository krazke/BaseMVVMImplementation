//
//  SecondViewModel.swift
//  BaseMVVMImplementation
//
//  Created by krazke on 2/7/19.
//  Copyright © 2019 krazke.corp. All rights reserved.
//

class SecondViewModel: BaseViewModel {
    
    var nameData: String?
    
    override init(services: BaseServicesConfiguration, sceneCoordinator: SceneCoordinator) {
        nameData = "SecondViewController"
        super.init(services: services, sceneCoordinator: sceneCoordinator)
    }
    
    func returnToFirstVC() {
        guard let name = nameData else { return }
        
        services.logger.log("Current view controller \(name)")
        sceneCoordinator.pop()
    }
}
