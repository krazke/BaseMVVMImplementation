//
//  SecondViewController.swift
//  BaseMVVMImplementation
//
//  Created by krazke on 2/7/19.
//  Copyright © 2019 krazke.corp. All rights reserved.
//

import UIKit

class SecondViewController: BaseViewController<SecondViewModel> {
    
    let label = UILabel()
    let button = UIButton()
    
    override func loadView() {
        super.loadView()
        
        view.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(40)
            make.right.equalToSuperview().offset(-40)
            make.height.equalTo(32)
        }
        
        view.addSubview(button)
        button.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(40)
            make.right.bottom.equalToSuperview().offset(-40)
            make.height.equalTo(32)
        }
    }
    
    override func setupView() {
        view.backgroundColor = .yellow
        
        label.font = UIFont(name: "HelveticaNeue", size: 14)
        label.textColor = .black
        label.textAlignment = .center
        
        button.setTitle("Return to first VC", for: .normal)
        button.setTitleColor(.blue, for: .normal)
        button.addTarget(self, action: #selector(returnToFirstVC), for: .touchUpInside)
    }
    
    override func localize() {
        label.text = model.nameData
    }
    
    @objc func returnToFirstVC() {
        model.returnToFirstVC()
    }
}
