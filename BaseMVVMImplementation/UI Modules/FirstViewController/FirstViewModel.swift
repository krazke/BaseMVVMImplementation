//
//  FirstViewModel.swift
//  BaseMVVMImplementation
//
//  Created by krazke on 2/7/19.
//  Copyright © 2019 krazke.corp. All rights reserved.
//

class FirstViewModel: BaseViewModel {
    
    var nameData: String?
    
    override init(services: BaseServicesConfiguration, sceneCoordinator: SceneCoordinator) {
        nameData = "FirstViewController"
        
        super.init(services: services, sceneCoordinator: sceneCoordinator)
    }
    
    func presentSecondVC() {
        guard let name = nameData else { return }
        
        services.logger.log("Current view controller \(name)")
        
        let secondViewModel = SecondViewModel(services: services, sceneCoordinator: sceneCoordinator)
        let secondScene = Scene.second(secondViewModel)
        
        sceneCoordinator.transition(to: secondScene, type: .push)
    }
}
