//
//  LoggerServiceV1.swift
//  BaseMVVMImplementation
//
//  Created by krazke on 2/7/19.
//  Copyright © 2019 krazke.corp. All rights reserved.
//

class LoggerServiceV1: LoggerService {
    
    func log(_ message: String) {
        print(message)
    }
}
