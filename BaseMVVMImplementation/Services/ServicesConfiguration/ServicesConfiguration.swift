//
//  ServicesConfiguration.swift
//  BaseMVVMImplementation
//
//  Created by krazke on 2/7/19.
//  Copyright © 2019 krazke.corp. All rights reserved.
//

class ServicesConfiguration {
    
    // MARK: - Public services
    
    public let logger: LoggerService
    
    // MARK: - Private services
    
    // MARK: - Init
    
    public init() {
        self.logger = LoggerServiceV1()
    }
}
