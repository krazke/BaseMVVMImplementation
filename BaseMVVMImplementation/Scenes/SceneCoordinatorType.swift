//
//  SceneCoordinatorType.swift
//  BaseMVVMImplementation
//
//  Created by krazke on 2/7/19.
//  Copyright © 2019 krazke.corp. All rights reserved.
//

import UIKit

protocol SceneCoordinatorType {
    
    func transition(to scene: Scene, type: SceneTransitionType, animated: Bool)
    func pop(animated: Bool)
    func dismiss(animated: Bool)
    func showAlert(alert: UIAlertController, animated: Bool)
}

extension SceneCoordinatorType {
    
    func pop() {
        return pop(animated: true)
    }
    
    func dismiss() {
        return dismiss(animated: true)
    }
}
