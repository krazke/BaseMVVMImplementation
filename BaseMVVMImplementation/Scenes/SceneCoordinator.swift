//
//  SceneCoordinator.swift
//  BaseMVVMImplementation
//
//  Created by krazke on 2/7/19.
//  Copyright © 2019 krazke.corp. All rights reserved.
//

import Foundation
import UIKit

class SceneCoordinator: NSObject, SceneCoordinatorType {
    
    fileprivate var window: UIWindow
    
    fileprivate var currentViewController: UIViewController {
        
        var current = window.rootViewController
        
        if let tabbarController = current as? UITabBarController {
            current = tabbarController.selectedViewController
        }
        
        if let navigationController = current as? UINavigationController {
            current = navigationController.topViewController
        }
        
        while current?.presentedViewController != nil {
            if !(current?.presentedViewController is UISearchController) {
                current = current?.presentedViewController
                
                if let navigationController = current as? UINavigationController {
                    current = navigationController.topViewController
                }
            }
        }
        
        return current!
    }
    
    required init(window: UIWindow) {
        self.window = window
    }
    
    static func actualViewController(for viewController: UIViewController) -> UIViewController {
        if let tabbarController = viewController as? UITabBarController {
            return actualViewController(for: tabbarController.selectedViewController!)
        } else if let navigationController = viewController as? UINavigationController {
            return navigationController.viewControllers.first!
        } else {
            return viewController
        }
    }
    
    func transition(to scene: Scene, type: SceneTransitionType, animated: Bool = true) {
        
        let viewController = scene.viewController()
        
        switch type {
        case .root:
            if let fromVC = window.rootViewController, animated {
                viewController.view.frame = fromVC.view.frame
                UIView.transition(from: fromVC.view,
                                  to: viewController.view,
                                  duration: 0.33,
                                  options: [.transitionCrossDissolve, .curveEaseOut],
                                  completion: { [unowned self] _ in
                                    self.window.rootViewController = viewController
                })
            } else {
                window.rootViewController = viewController
            }
            
        case .push:
            guard let navigationController = currentViewController.navigationController else {
                fatalError("Can't push a view controller without a current navigation controller")
            }
            navigationController.pushViewController(viewController, animated: animated)
            
        case .modal:
            currentViewController.present(viewController, animated: animated, completion: nil)
        }
    }
    
    func pop(animated: Bool = true) {
        if let navigationController = currentViewController.navigationController,
            navigationController.viewControllers.count > 1 {
            guard navigationController.popViewController(animated: animated) != nil else { return }
        } else
            if currentViewController.presentingViewController != nil {
                // dismiss a modal controller
                currentViewController.dismiss(animated: animated, completion: nil)
        }
    }
    
    func dismiss(animated: Bool) {
        currentViewController.dismiss(animated: animated)
    }
    
    func dismissToRoot(animated: Bool) {
        
        var viewController: UIViewController? = currentViewController
        while viewController!.presentingViewController != nil {
            viewController = viewController!.presentingViewController
        }
        
        viewController!.dismiss(animated: true, completion: nil)
    }
    
    func showAlert(alert: UIAlertController, animated: Bool) {
        
        currentViewController.present(alert, animated: animated, completion: nil)
    }
}

extension SceneCoordinator: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
