//
//  Scene+ViewController.swift
//  BaseMVVMImplementation
//
//  Created by krazke on 2/7/19.
//  Copyright © 2019 krazke.corp. All rights reserved.
//

import UIKit

extension Scene {
    
    func viewController() -> UIViewController {
        
        switch self {
            
        case .navigation(let model):
            return BaseNavigationViewController(model: model)
        
        case .first(let model):
            let viewController = FirstViewController()
            viewController.setModel(model)
            return viewController
            
        case .second(let model):
            let viewController = SecondViewController()
            viewController.setModel(model)
            return viewController
        }
    }
}
