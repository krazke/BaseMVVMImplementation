//
//  Scene.swift
//  BaseMVVMImplementation
//
//  Created by krazke on 2/7/19.
//  Copyright © 2019 krazke.corp. All rights reserved.
//

enum Scene {
    
    case navigation(BaseNavigationControllerModel)
    
    case first(FirstViewModel)
    case second(SecondViewModel)
}
