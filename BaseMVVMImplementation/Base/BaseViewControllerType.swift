//
//  BaseViewControllerType.swift
//  BaseMVVMImplementation
//
//  Created by krazke on 2/7/19.
//  Copyright © 2019 krazke.corp. All rights reserved.
//

protocol BaseViewControllerType {
    
    associatedtype Model: BaseViewModel
    
    var model: Model! { get }
}
