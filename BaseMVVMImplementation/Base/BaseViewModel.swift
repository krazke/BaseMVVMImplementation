//
//  BaseViewModel.swift
//  BaseMVVMImplementation
//
//  Created by krazke on 2/7/19.
//  Copyright © 2019 krazke.corp. All rights reserved.
//

class BaseViewModel {
    
    let services: BaseServicesConfiguration
    let sceneCoordinator: SceneCoordinator
    
    init(services: BaseServicesConfiguration, sceneCoordinator: SceneCoordinator) {
        self.services = services
        self.sceneCoordinator = sceneCoordinator
    }
}
