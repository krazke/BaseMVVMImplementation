//
//  BaseViewController.swift
//  BaseMVVMImplementation
//
//  Created by krazke on 2/7/19.
//  Copyright © 2019 krazke.corp. All rights reserved.
//

import UIKit
import SnapKit

class BaseViewController<T: BaseViewModel>: UIViewController, BaseViewControllerType {
    
    typealias Model = T
    
    private (set) internal var model: Model!
    
    func setModel(_ model: Model!) {
        self.model = model
    }
    
    override func loadView() {
        super.loadView()
        
        setupConstraints()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        localize()
        setupView()
        setupTable()
        setupCollection()
    }
    
    func localize() {}
    func setupView() {}
    func setupTable() {}
    func setupCollection() {}
    func setupConstraints() {}
}
