//
//  BaseNavigationControllerModel.swift
//  BaseMVVMImplementation
//
//  Created by krazke on 2/7/19.
//  Copyright © 2019 krazke.corp. All rights reserved.
//

class BaseNavigationControllerModel: BaseViewModel {
    
    let root: Scene
    let config: NavigationConfig
    
    struct NavigationConfig {
        
        var hideBar: Bool
        var large: Bool
        
        static let hidden = NavigationConfig(hideBar: true, large: false)
        static let regular = NavigationConfig(hideBar: false, large: false)
        static let large = NavigationConfig(hideBar: false, large: true)
    }
    
    init(services: BaseServicesConfiguration, sceneCoordinator: SceneCoordinator,
         root: Scene, config: NavigationConfig) {
        self.root = root
        self.config = config
        super.init(services: services, sceneCoordinator: sceneCoordinator)
    }
}
