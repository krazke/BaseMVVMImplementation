//
//  BaseNavigationViewController.swift
//  BaseMVVMImplementation
//
//  Created by krazke on 2/7/19.
//  Copyright © 2019 krazke.corp. All rights reserved.
//

import UIKit

class BaseNavigationViewController: UINavigationController, BaseViewControllerType {
    
    var backButton = UIBarButtonItem(title: nil, style: .plain, target: nil, action: nil)
    
    typealias Model = BaseNavigationControllerModel
    
    var model: Model!
    
    init(model: Model) {
        
        super.init(navigationBarClass: BaseNavigationBar.self, toolbarClass: UIToolbar.self)
        self.model = model
        
        let config = model.config
        
        setNavigationBarHidden(config.hideBar, animated: false)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = false
        let rootViewController = model.root.viewController()
        setViewControllers([rootViewController], animated: false)
        
        if #available(iOS 11.0, *) {
            navigationBar.prefersLargeTitles = config.large
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .default
    }
    
    override var prefersStatusBarHidden: Bool {
        return topViewController?.prefersStatusBarHidden ?? false
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
